import java.util.Scanner;

public class LearningTodoApp {
   public static String[] model = new String[10];
   public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        viewTodo();
    }

    static void showTodo(){
        System.out.println("=== ->> TODO <<- === ");
        for (var i=0; i< model.length;i++){
            var todo = model[i];
            var no = i+1;

            if (todo!=null){
                System.out.println(no+". "+todo);
            }
        }
    }

    static void showTodoTest(){
        for(var i=0; i<10;i++){
            model[i]="agung";
        }
        showTodo();
    }

    static void addList(String data){
//        cek apakah datanya penuh
        var penuh = true;
        for (var i = 0 ; i< model.length;i++){
            if(model[i]==null){
                penuh=false;
                break;
            }
        }
//        resize ukuran model, jika sudah penuh
        var tem=model;
        model = new String[model.length*2];
        for (var i=0;i<tem.length;i++){
            model[i]=tem[i];
        }

//        tambah data
        for (var i = 0; i<model.length;i++){
            if(model[i]==null){
              model[i]=data;
                break;
            }
        }

    }

    static void addListTest(){
        model[0]="agung";
        model[1]="prastia";
        addList("s.kom");
        showTodo();
    }


    static boolean delList(Integer number){
//        cek apakah number yg dimasukkan ada
        if(number-1>= model.length){
            return false;
        }else if (model[number-1]==null){
            return false;
        }else{
        for (var i =(number-1);i< model.length;i++){
                if (i == (model.length-1)){
                    model[i]=null;
                }else{
                    model[i]=model[i+1];
                }
        }
            return true;
        }
    }

    /**
     * method inputan
     */
    static String input(String info){
        System.out.print(info+" : ");
        var data = scanner.nextLine();
        return data;
    }

    static void delListTest(){
        model[0]="agung";
        model[1]="prasetia";
        model[2]="skom";
        delList(1);
        showTodo();
    }
    static void viewTodo(){
        while (true){
            showTodo();
            System.out.println("=== ->> MENU <<- ===");
            System.out.println("1. Tambah TODO\n" +
                    "2. Delete TODO\n" +
                    "3. keluar (x)");
            var input = input("pilih");
            if (input.equals("1")){
                viewAdd();
            }else if (input.equals("2")){
                viewDel();
            }else if(input.equals("x")){
                break;
            }
            else{
                System.out.println("inputan tidak valid!");
            }
        }

    }

    /**
     * menampilkan viewdelete
     */
    static void viewAdd(){
        System.out.println("=== TAMBAH TODO ===");
        var data = input("masukkan inputan (x jika batal)");
        if (data.equals("x")){
        }else{
            addList(data);
        }
    }

    static void viewDel(){
        System.out.println("=== HAPUS TODO ===");
        var data = input("masukkan inputan (x jika batal)");
        if (data.equals("x")){
        }else{
            var s = Integer.parseInt(data);
            boolean success = delList(s);
            if(!success){
                System.out.println("gagal menghapus!");
            }
        }
    }
}
